module.exports = {
  '**/*.(js|jsx|ts|tsx|json)': () => [`npm run prettier --write`],
};
