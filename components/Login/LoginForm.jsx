import { signIn } from 'next-auth/react';
import { useState } from 'react';

export default function LoginForm({ error }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleEmailChange(event) {
    setEmail(event.target.value);
  }

  function handlePasswordChange(event) {
    setPassword(event.target.value);
  }

  async function submitLogin(event) {
    event.preventDefault();
    await signIn('credentiallogin', {
      email: email,
      password: password,
    });
  }

  return (
    <>
      {!!error && (
        <div role="alert">
          <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
            <p>
              {error.split(':')[0]} : {error.split(':')[1]}
            </p>
          </div>
        </div>
      )}
      <form method={'post'} onSubmit={submitLogin}>
        <div className="form-group pb-2">
          <label htmlFor="email">
            <small>Email</small>
          </label>
          <input
            type="email"
            name={'email'}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            defaultValue={email}
            onKeyUp={handleEmailChange}
          />
        </div>
        <div className="form-group pb-4">
          <label htmlFor="password">
            <small>Password</small>
          </label>
          <input
            type="password"
            name={'password'}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            defaultValue={password}
            onKeyUp={handlePasswordChange}
          />
        </div>
        <button
          type="submit"
          className="w-full bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white px-4 border border-blue-500 hover:border-transparent rounded"
        >
          <small>Login</small>
        </button>
      </form>
    </>
  );
}
