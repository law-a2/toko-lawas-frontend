import Navbar from '@components/Navbar/Navbar';

export default function Layout({ children }) {
  return (
    <>
      <Navbar />
      <div className="min-h-screen bg-[#EFF2F6]">{children}</div>
    </>
  );
}
