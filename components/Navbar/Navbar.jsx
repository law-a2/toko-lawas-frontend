import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import NavbarAuthenticated from './NavbarAuthenticated';
import NavbarNotAuthenticated from './NavbarNotAuthenticated';

export default function Navbar() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [name, setName] = useState('');
  const { data: session, status } = useSession();
  const loading = status === 'loading';

  useEffect(() => {
    const handleSession = () => {
      if (!loading && session) {
        setIsAuthenticated(true);
        session.user.name
          ? setName(session.user.name)
          : setName(session.user.first_name + ' ' + session.user.last_name);
      }
    };
    handleSession();
  }, [session, loading]);

  console.log(session);
  return (
    <nav className="px-2 sm:px-4 py-2.5 dark:bg-black">
      <div className="container flex flex-wrap justify-between items-center mx-auto">
        <div href="" className="flex items-center">
          <Link href="/" passHref={true}>
            <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
              Toko LAWas
            </span>
          </Link>
          <Link href="/product/list-user">
            <a className="ml-4 dark:text-white">Shop</a>
          </Link>
          {isAuthenticated && <Link href="/transaction">
            <a className="ml-4 dark:text-white">Transaction</a>
          </Link>}
        </div>
        <div className="flex md:order-2 space-x-4">
          {isAuthenticated ? (
            <NavbarAuthenticated name={name} />
          ) : (
            <NavbarNotAuthenticated />
          )}
          <button
            data-collapse-toggle="mobile-menu-4"
            type="button"
            className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
            aria-controls="mobile-menu-4"
            aria-expanded="false"
          >
            <span className="sr-only">Open main menu</span>
            <svg
              className="w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                clipRule="evenodd"
              ></path>
            </svg>
            <svg
              className="hidden w-6 h-6"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              ></path>
            </svg>
          </button>
        </div>
      </div>
    </nav>
  );
}
