import Link from 'next/link';
import {BsFillBellFill} from 'react-icons/bs'

export default function NavbarAuthenticated(props) {
  const { name } = props;
  return (
    <>
      <Link href="/notification" passHref={true}>
        <a className="pt-1">
        <BsFillBellFill className="text-white"/>
        </a>
      </Link>
      <Link href="#" passHref={true}>
        <a className="text-white">{name}</a>
      </Link>
      <Link href="/logout" passHref={true}>
        <a className="text-[red]">Logout</a>
      </Link>
    </>
  );
}
