import Link from 'next/link';

export default function NavbarNotAuthenticated() {
  return (
    <Link href="/login" passHref={true}>
      <a className="text-white">Login</a>
    </Link>
  );
}
