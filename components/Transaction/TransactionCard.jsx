import axios from "axios";
import Image from "next/image";
import { useEffect, useState } from "react";

export default function TransactionCard({ transaction }) {
    const [product, setProduct] = useState({})
    useEffect(() => {
        axios.get('http://35.223.24.101:4000/product/' + transaction.idStock)
            .then((response) => setProduct(response.data.data))
            .catch(() => setProduct({
                Name: "",
                Picture: "",
                Price: "/placeholder.png"
            }))
    }, [transaction.idStock])
    
    return <div className={"bg-white w-100 rounded p-3 mb-3"}>
        <div className="flex">
            <div className="relative" style={{height:"80px", width:"80px"}}>
                <Image src={product.Picture ? product.Picture : "/placeholder.png"} layout="fill" objectFit="cover" alt="product_pic"/>
            </div>
            <div className="flex flex-column ms-3">
                <div className="font-bold">{product.Name !== "" ? product.Name : "Produk tidak diketahui"}</div>
                <div className="font-light">Price: {product.Name !== "" ? product.Price : ""}</div>
                <div className="font-light">Quantity: {product.Name !== "" ? transaction.jumlah : ""}</div>
            </div>
        </div>
    </div>
}