import axios from 'axios';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import styles from '../../styles/Home.module.css';

export default function TransactionCardAdmin({ transaction }) {
  const [product, setProduct] = useState({});
  useEffect(() => {
    axios
      .get('http://35.223.24.101:4000/product/' + transaction.idStock)
      .then((response) => setProduct(response.data.data))
      .catch(() =>
        setProduct({
          Name: '',
          Picture: '',
          Price: '/placeholder.png',
        })
      );
  }, [transaction.idStock]);

  const onConfirm = () => {
    // update transaction status
    console.log('transaction', transaction);
    const updateTransaction = {
      method: 'put',
      url: `34.143.171.38:100/api/v1/transaction/${transaction.id}`,
      data: {
        idStock: transaction.idStock,
        username: transaction.username,
        harga: transaction.harga,
        jumlah: transaction.jumlah,
        status: 'CONFIRMED',
      },
    };
    axios(updateTransaction)
      .alert('Transaction confirmed')
      .catch((error) => {
        alert(error);
        alert('error when updating transaction data');
      });
  };

  return (
    <div className={'bg-white w-100 rounded p-3 mb-3'}>
      <div className="flex">
        <div className="relative" style={{ height: '80px', width: '80px' }}>
          <Image
            src={product.Picture ? product.Picture : '/placeholder.png'}
            layout="fill"
            objectFit="cover"
            alt="product_pic"
          />
        </div>
        <div className="flex flex-column ms-3 flex-grow-1">
          <div className="font-bold">
            {product.Name !== '' ? product.Name : 'Produk tidak diketahui'}
          </div>
          <div className="font-light text-sm">By {transaction.username}</div>
          <div className="font-light text-sm">
            Price: {product.Name !== '' ? product.Price : ''}
          </div>
          <div className="font-light text-sm">
            Quantity: {product.Name !== '' ? transaction.jumlah : ''}
          </div>
        </div>
        <div className="flex flex-column start-end">
          <Button
            variant="primary"
            className={styles.primaryButton}
            onClick={onConfirm}
          >
            Confirm
          </Button>
        </div>
      </div>
    </div>
  );
}
