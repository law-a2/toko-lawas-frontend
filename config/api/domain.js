const conf = {
  production: {
    PROFILE_BASE_URL: 'http://34.143.171.38',
    STOCK_BASE_URL: 'http://35.223.24.101:4000',
    BASE_URL: '/api/v1',
    GATEWAY_URL: 'http://34.143.171.38:90',
  },
  develop: {
    PROFILE_BASE_URL: 'http://localhost:8000',
    STOCK_BASE_URL: 'http://localhost:8080',
    BASE_URL: '/api/v1',
    GATEWAY_URL: 'http://34.143.171.38:90',
  },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  ...conf.base,
  ...conf[process.env.NEXT_PUBLIC_PHASE || process.env.NODE_ENV],
};
