[![Netlify Status](https://api.netlify.com/api/v1/badges/5513f56d-9fdd-4cde-a9fb-41f348f4f6da/deploy-status)](https://app.netlify.com/sites/tokolawas/deploys)

# Toko LAWas

Toko LAWas adalah toko-tokoan hasil kerjaan tugas kelompok LAW

## Kelompok A2

| No  | Nama                        | NPM        |
| --- | --------------------------- | ---------- |
| 1.  | Ageng Anugrah Wardoyo Putra | 1906398212 |
| 2.  | Ian Andersen Ng             | 1906400280 |
| 3.  | Niti Cahyaning Utami        | 1906350894 |
| 4.  | Rhendy Rivaldo              | 1706039982 |
| 5.  | Samuel                      | 1906285592 |

## How to Run

**Note: Please use npm**

- Open terminal and install all dependencies :

```bash
npm install
```

- Go to project root directory and run the development server:

```bash
npm run dev
```

- Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
