import TransactionCard from '@components/Transaction/TransactionCard';
import axios from 'axios';
import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import styles from '../../styles/Home.module.css';

export default function Transaction() {
  const { data: session } = useSession();
  const [transactions, setTransaction] = useState([]);

  useEffect(() => {
    axios
      .get(
        'http://34.143.171.38:100/api/v1/transaction/' + session?.data?.username
      )
      .then((response) =>
        setTransaction(response.data.data.transactions?.reverse())
      );
  }, [session?.data?.username]);

  return (
    <main>
      <div className="container pb-8">
        <div className="text-center">
          <h1 className={styles.heading}>My Transactions</h1>
        </div>
        {transactions?.map((transaction, index) => (
          <div key={index}>
            <TransactionCard transaction={transaction} />
          </div>
        ))}
      </div>
    </main>
  );
}
