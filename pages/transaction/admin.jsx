import TransactionCardAdmin from '@components/Transaction/TransactionCardAdmin';
import axios from 'axios';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import styles from '../../styles/Home.module.css';

export default function TransactionAdmin() {
  const { data: session } = useSession();
  const [transactions, setTransaction] = useState([]);
  const router = useRouter();

  useEffect(() => {
    if (session) {
      if (!session?.data?.is_superuser) {
        router.push('/');
      }
    }
    axios
      .get(
        'http://localhost:8080/api/v1/transaction/' + session?.data?.username
      )
      .then((response) =>
        setTransaction(response.data.data.transactions?.reverse())
      );
  }, [session, router]);

  return (
    <main>
      <div className="container pb-8">
        <div className="text-center">
          <h1 className={styles.heading}>All Transactions</h1>
        </div>
        {transactions?.map((transaction, index) => (
          <div key={index}>
            <TransactionCardAdmin transaction={transaction} />
          </div>
        ))}
      </div>
    </main>
  );
}
