import { signIn } from 'next-auth/react';
import Head from 'next/head';
import { BsGoogle } from 'react-icons/bs';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import RegisterForm from '@components/Register/RegisterForm';
import Link from 'next/link';

function Register() {
  const { data: session, status } = useSession();
  const loading = status === 'loading';
  const router = useRouter();

  let { error } = router.query;
  error = error && error.toLowerCase().includes('login') ? error : false;

  const handleSession = () => {
    if (!loading && session) {
      router.push('/');
    }
  };

  return (
    <>
      {session && handleSession()}
      <Head>
        <title>Register</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="pt-8 flex justify-center items-center">
        <div className="container bg-white w-[30%] min-w-max p-6 leading-10">
          <h2 className="text-3xl font-medium">Welcome</h2>
          <p className="text-gray-500">Register an Account</p>
          <RegisterForm error={error} />
          <div className="text-sm text-center mt-2">
            Already Have Account?
            <Link href="/login" passHref>
              <a className="font-bold text-blue-700"> Login</a>
            </Link>
          </div>
          <div className="mt-4 text-center">OR</div>
          <button
            className="border border-gray-400 rounded text-center w-full my-4 flex items-center justify-center space-x-4"
            onClick={() => signIn('google')}
          >
            <BsGoogle /> <p>Continue with Google</p>
          </button>
        </div>
      </div>
    </>
  );
}

export default Register;
