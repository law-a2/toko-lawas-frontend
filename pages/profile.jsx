import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import styles from '../styles/Home.module.css';

export default function Shop() {
  var history = [];
  const title = 'Profile';
  for (var i = 0; i < 6; i++) {
    history.push({
      product: 'Product X',
      price: 'Rp 123',
    });
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>{title} | Toko LAWas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="text-center">
          <h1 className={styles.heading}>{title}</h1>
        </div>
        <div className="flex flex-row space-x-4 mx-4">
          <section>
            <div className="bg-white w-60 h-60">
              <Image src="" layout={"fill"} objectFit={"cover"} alt="profile-pic" />
            </div>
          </section>
          <section className="flex flex-col flex-grow">
            {history.map((item, index) => (
              <div style={style.card} key={index}>
                <span className="font-bold">{item.product}</span>
                <span>{item.price}</span>
                <button className={styles.button}>
                  <Link href={'/'}>Buy Again</Link>
                </button>
              </div>
            ))}
          </section>
        </div>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by Seblak
        </a>
      </footer>
    </div>
  );
}

const style = {
  card: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    grow: 1,
    justifyContent: 'center',
    borderRadius: '5px',
    padding: '1rem',
    marginBottom: '1rem',
    backgroundColor: '#fff',
    boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
  },
};
