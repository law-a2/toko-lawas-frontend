import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Home.module.css';

export default function Shop() {
  var cartItems = [];
  var totalPrice = 0;
  const title = 'Your Cart';

  for (var i = 0; i < 3; i++) {
    cartItems.push({
      product: 'Product X',
      qty: 2,
      price: 123,
    });
    totalPrice += cartItems[i].price * cartItems[i].qty;
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>{title} | Toko LAWas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="text-center">
          <h1 className={styles.heading}>{title}</h1>
        </div>
        <div className="flex flex-row">
          <section className="flex flex-col flex-grow">
            {cartItems.map((item, index) => (
              <>
                <div style={style.item} key={index}>
                  <span className="font-bold">{item.product}</span>
                  <span>Quantity: {item.qty}</span>
                  <span>Rp {item.price}</span>
                  <button className={styles.button}>
                    <Link href={'/'}>Remove</Link>
                  </button>
                </div>
                {index < cartItems.length - 1 && <hr style={style.divider} />}
              </>
            ))}
          </section>
          <section className="flex flex-col flex-grow space-y-4">
            <div className="font-bold">Order Summary</div>
            <div>Rp {totalPrice}</div>
            <hr style={style.divider} />
            <div>
              <button style={style.wideButton}>
                <Link href={'/'}>Checkout</Link>
              </button>
            </div>
          </section>
        </div>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by Seblak
        </a>
      </footer>
    </div>
  );
}

const style = {
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    grow: 1,
    justifyContent: 'center',
    borderRadius: '5px',
    padding: '1rem',
  },
  wideButton: {
    backgroundColor: '#222',
    borderRadius: '5px',
    padding: '0.5rem',
    width: '100%',
    cursor: 'pointer',
    fontSize: '0.8rem',
    fontWeight: 'bold',
    color: '#fff',
    textTransform: 'uppercase',
  },
  divider: {
    borderBottom: '1px solid #bbb',
  },
};
