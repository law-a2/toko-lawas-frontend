import { signIn } from 'next-auth/react';
import Head from 'next/head';
import { BsGoogle } from 'react-icons/bs';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import LoginForm from '@components/Login/LoginForm';
import Link from 'next/link';

function Login() {
  const { data: session, status } = useSession();
  const loading = status === 'loading';
  const router = useRouter();

  let { error } = router.query;
  error = error && error.toLowerCase().includes('login') ? error : false;

  const handleSession = () => {
    if (!loading && session) {
      router.push('/');
    }
  };

  return (
    <>
      {session && handleSession()}
      <Head>
        <title>Login</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="pt-8 flex justify-center items-center">
        <div className="container bg-white w-[30%] min-w-max p-6 leading-10">
          <h2 className="text-3xl font-medium">Welcome Back</h2>
          <p className="text-gray-500">Login to continue</p>
          <LoginForm error={error} />
          <div className="text-sm text-center mt-2">
            Don&apos;t have an account?
            <Link href="/register" passHref>
              <a className="font-bold text-blue-700"> Register</a>
            </Link>
          </div>
          <div className="mt-4 text-center">OR</div>
          <button
            className="border border-gray-400 rounded text-center w-full my-4 flex items-center justify-center space-x-2"
            onClick={() => signIn('google')}
          >
            <BsGoogle /> <div>Continue with Google</div>
          </button>
        </div>
      </div>
    </>
  );
}

export default Login;
