import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { SessionProvider } from 'next-auth/react';
import Head from 'next/head';
import Layout from '@components/Layout';

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <SessionProvider session={session}>
      <CustomHead />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </SessionProvider>
  );
}

const CustomHead = () => {
  const title = 'Toko LAWas';
  const desc = 'Toko bukan sembarang toko, ini toko tapi sembarangan';
  return (
    <Head>
      <meta name="description" content={desc} />
      <meta name="title" content={title} />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={desc} />

      {/* <!-- Twitter --> */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content="#" />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={desc} />

      <meta name="apple-mobile-web-app-title" content={title} />
      <meta name="application-name" content={title} />
      <meta name="msapplication-TileColor" content="#2b5797" />
      <meta name="theme-color" content="#f1f3f6" />
    </Head>
  );
};

export default MyApp;
