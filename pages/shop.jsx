import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Link from 'next/link';

export default function Shop() {
  const title = 'Shop';
  var shopItems = [];

  for (var i = 0; i < 3; i++) {
    shopItems.push({
      product: 'Product X',
      price: 123,
    });
  }

  const SearchBox = () => {
    return (
      <div className="flex flex-row justify-center my-8">
        <input type="text" style={style.searchBox} placeholder="Search" />
        <button className={styles.button}>Search</button>
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>{title} | Toko LAWas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="text-center">
          <h1 className={styles.heading}>{title}</h1>
        </div>
        <SearchBox></SearchBox>
        <div className="flex flex-row space-x-4">
          <section className="flex flex-grow mb-8 justify-center">
            {shopItems.map((item, index) => (
              <div style={style.item} key={index}>
                <span className="font-bold">{item.product}</span>
                <span>Rp {item.price}</span>
                <button className={styles.button}>
                  <Link href={''}>Add to Cart</Link>
                </button>
              </div>
            ))}
          </section>
        </div>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by Seblak
        </a>
      </footer>
    </div>
  );
}

const style = {
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '5px',
    padding: '1rem',
  },
  searchBox: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'space-between',
    backgroundColor: '#fff',
    borderRadius: '0.5rem',
    paddingLeft: '1rem',
    width: '80%',
  },
};
