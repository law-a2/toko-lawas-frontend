import Head from 'next/head';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Toko LAWas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="">Toko LAWas</a>
        </h1>

        <p className={styles.description}>
          Get started by <code className={styles.code}>ngeluh</code>
        </p>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by Seblak
        </a>
      </footer>
    </div>
  );
}
