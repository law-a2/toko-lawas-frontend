import axios from 'axios';
import NextAuth from 'next-auth';
import GoogleProvider from 'next-auth/providers/google';
import CredentialProvider from 'next-auth/providers/credentials';

export default NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
    CredentialProvider({
      id: 'credentiallogin',
      name: 'Credential Login',
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        const { email, password } = credentials;
        let response;
        let error;
        await axios
          .post(process.env.NEXT_API_URL + 'api/v1/auth/login/', {
            email,
            password,
          })
          .then((e) => (response = e))
          .catch(function (errorResponse) {
            error = errorResponse;
          });
        if (error) {
          throw new Error('Login Failed:the username or password is invalid');
        } else if (response && response.data) {
          return response.data;
        }
        console.log(response);
        return null;
      },
    }),
    CredentialProvider({
      id: 'credentialregister',
      name: 'Credential Register',
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        const { email, first_name, last_name, password } = credentials;
        let response;
        let error;
        await axios
          .post(process.env.NEXT_API_URL + 'api/v1/auth/register/', {
            email,
            first_name,
            last_name,
            password,
          })
          .then((e) => (response = e))
          .catch(function (errorResponse) {
            error = errorResponse;
          });
        if (error) {
          throw new Error('Register failed:' + error.response.data.message);
        } else if (response && response.data) {
          return response.data;
        }
        return null;
      },
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async signIn({ user, account }) {
      if (account.provider === 'google') {
        const { access_token, id_token } = account;
        try {
          await axios
            .post(process.env.NEXT_API_URL + 'api/v1/auth/google/', {
              access_token: access_token,
              id_token: id_token,
            })
            .then((res) => {
              user.access_token = res.data.access_token;
              user.data = res.data.user;
              return user;
            });
          return true;
        } catch (error) {
          console.log(error);
          return false;
        }
      } else if (account.provider === 'credentiallogin') {
        return true;
      } else if (account.provider === 'credentialregister') {
        return true;
      }

      return false;
    },
    async jwt({ token, user }) {
      if (user) {
        token.access_token = user.access_token;
        token.user_data = user.data;
        for (const [key, value] of Object.entries(user)) {
          token[key] = value;
        }
      }
      return token;
    },
    async session({ session, token }) {
      session.access_token = token.access_token;
      session.user_data = token.user_data;
      for (const [key, value] of Object.entries(token)) {
        session[key] = value;
      }
      return session;
    },
  },
  pages: {
    signIn: '/login',
  },
});
