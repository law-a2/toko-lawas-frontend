import axios from 'axios';

const PRODUCTION_URL = 'http://35.223.24.101:4000';

const axiosFormClient = axios.create({
  baseURL: `${PRODUCTION_URL}`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
});

export const apiCreateProduct = (fd) => {
  return axiosFormClient.post(`/product`, fd);
};
