import React, { useEffect } from 'react';
import { signOut } from 'next-auth/react';

const Logout = () => {
  useEffect(() => {
    signOut({ callbackUrl: '/' });
  }, []);

  return <></>;
};

export default Logout;
