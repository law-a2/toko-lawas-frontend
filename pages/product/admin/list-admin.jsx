import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Row, Col } from 'react-bootstrap';

function AdminProductList() {
  const [productData, setProductData] = useState([]);
  const [isDataFetched, setIsDataFetched] = useState(false);

  useEffect(() => {
    const getProductList = () => {
      const config = {
        method: 'get',
        url: 'http://35.223.24.101:4000/product',
      };
      axios(config)
        .then((response) => {
          setProductData(response.data.data);
          console.log(response.data.data);
        })
        .catch(() => {
          alert('error when fetching product data');
        });
    };

    if (!productData.length && !isDataFetched) {
      getProductList();
    }
  }, [productData, isDataFetched]);

  const handleDelete = (id) => {
    const config = {
      method: 'delete',
      url: `http://35.223.24.101:4000/product/${id}`,
    };
    console.log('masuk ke delete');

    axios(config)
      .then(() => {
        console.log('delete berhasil terpanggil');
        setTimeout(() => {
          setIsDataFetched(false);
          setProductData([]);
        }, 300);
      })
      .catch(() => {
        alert('Data gagal dihapus');
      });
  };

  return (
    <Container>
      <h1 className="text-center my-5">List Produk</h1>

      <Row>
        <Col className="d-flex justify-content-center justify-content-md-end">
          <Button variant="success">Add Product</Button>
        </Col>
      </Row>

      <Row>
        {productData.map((item, i) => (
          <Card key={i} style={{ width: '15rem' }} className="mb-3 mx-5">
            <Card.Img variant="top" src={item.Picture} />
            <Card.Body>
              <Card.Text style={{ fontSize: '18px' }}>{item.Name}</Card.Text>
              <Card.Text style={{ fontSize: '17px', fontWeight: 'bold' }}>
                Rp.{item.Price}
              </Card.Text>
              <Card.Text style={{ fontSize: '18px' }}>
                Jumlah : {item.Quantity}
              </Card.Text>
              <a onClick={() => handleDelete(item.ID)}>
                <Button variant="danger">Delete Produk</Button>
              </a>
            </Card.Body>
          </Card>
        ))}
        ;
      </Row>
    </Container>
  );
}

export default AdminProductList;
