import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import conf from 'config/api/domain';

function CreateStock() {
  const router = useRouter();
  const [input, setInput] = useState({
    ID: '',
    Name: '',
    Picture: '',
    Price: 0,
    Quantity: 0,
  });

  const { data: session } = useSession();
  const [accessToken, setAccessToken] = useState('');

  useEffect(() => {
    if (session) {
      if (session?.data?.is_superuser) {
        setAccessToken(session?.access_token);
      } else {
        router.push('/');
      }
    }
  }, [session, router]);

  const handleChange = (event) => {
    let value = event.target.value;
    const name = event.target.name;

    if (name === 'Price' || name == 'Quantity') {
      value = parseInt(value);
    }
    console.log(name);
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const config = {
      method: 'post',
      url: `${conf.GATEWAY_URL}/api/v1/products`,
      data: input,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    console.log(config.data);
    axios(config).catch((error) => {
      alert(error);
    });
  };

  return (
    <Container>
      <Row className="pt-5">
        <Col className="d-flex justify-content-center">
          <h2>Create Product</h2>
        </Col>
      </Row>
      <Row className="mt-3 px-3 px-sm-5">
        <Col>
          <Form.Group className="mb-3">
            <Form.Label>Product ID</Form.Label>
            <Form.Control type="text" onChange={handleChange} name="ID" />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mt-3 px-3 px-sm-5">
        <Col>
          <Form.Group className="mb-3">
            <Form.Label>Product Name</Form.Label>
            <Form.Control type="text" onChange={handleChange} name="Name" />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mt-3 px-3 px-sm-5">
        <Col>
          <Form.Group className="mb-3">
            <Form.Label>Gambar produk Produk</Form.Label>
            <Form.Control type="text" onChange={handleChange} name="Picture" />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mt-3 px-3 px-sm-5">
        <Col>
          <Form.Group className="mb-3">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" onChange={handleChange} name="Price" />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mt-3 px-3 px-sm-5">
        <Col>
          <Form.Group className="mb-3">
            <Form.Label>Jumlah produk tersedia</Form.Label>
            <Form.Control
              type="number"
              onChange={handleChange}
              name="Quantity"
            />
          </Form.Group>
        </Col>
      </Row>
      <Col className="d-flex flex-column justify-content-center align-items-center">
        <Button onClick={handleSubmit}>Create</Button>
      </Col>
    </Container>
  );
}
export default CreateStock;
