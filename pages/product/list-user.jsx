import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Row } from 'react-bootstrap';
import Link from 'next/link';
import styles from '../../styles/Home.module.css';

function ListProductPage() {
  const [productData, setProductData] = useState([]);

  useEffect(() => {
    const getProductList = () => {
      const config = {
        method: 'get',
        url: 'http://35.223.24.101:4000/product',
      };
      axios(config)
        .then((response) => {
          setProductData(response.data.data);
          console.log(response.data.data);
        })
        .catch((error) => {
          alert(error);
          alert('error when fetching product data');
        });
    };

    if (!productData.length) {
      getProductList();
    }
  }, [productData]);

  return (
    <Container>
      <h1 className="text-center my-5">Shop</h1>
      <Row>
        {productData.map((item, i) => (
          <Card key={i} style={{ width: '15rem' }} className="mb-3 mx-5">
            <Card.Img variant="top" src={item.Picture} />
            <Card.Body>
              <Card.Text style={{ fontSize: '18px' }}>{item.Name}</Card.Text>

              <Card.Text style={{ fontSize: '17px', fontWeight: 'bold' }}>
                Rp.{item.Price}
              </Card.Text>

              <Card.Text style={{ fontSize: '18px' }}>
                Jumlah : {item.Quantity}
              </Card.Text>

              <Button variant="primary" className={styles.primaryButton}>
                <Link href={`/product/detail/${item.ID}`}>Detail Produk</Link>
              </Button>
            </Card.Body>
          </Card>
        ))}
      </Row>
    </Container>
  );
}

export default ListProductPage;
