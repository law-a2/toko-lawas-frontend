import axios from 'axios';
import styles from '../../../styles/Home.module.css';
import Head from 'next/head';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';

export default function Detail() {
  const router = useRouter();
  const title = 'Beli Produk';
  const ID = router.query.id;
  const { data: session } = useSession();
  const [username, setUsername] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [totalPrice, setTotalPrice] = useState(0);
  const [productData, setProductData] = useState([]);
  const dbStokPath = 'http://35.223.24.101:4000';

  const increment = () => {
    if (quantity < productData.Quantity) {
      setQuantity(quantity + 1);
      setTotalPrice(totalPrice + productData.Price);
    }
  };

  const decrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
      setTotalPrice(totalPrice - productData.Price);
    }
  };

  const onSubmit = () => {
    // reduce stock
    const reduceStock = {
      method: 'put',
      url: `${dbStokPath}/product/${productData.ID}`,
      data: {
        ID: productData.ID,
        Name: productData.Name,
        Picture: productData.Picture,
        Price: productData.Price,
        Quantity: parseInt(productData.Quantity) - parseInt(quantity),
      },
    };
    axios(reduceStock).catch((error) => {
      alert(error);
      alert('error when reducing product data');
    });

    // create transaction instance
    const config = {
      method: 'post',
      url: 'http://34.143.171.38:100/api/v1/transaction',
      data: {
        idStock: productData.ID,
        username: username,
        harga: parseInt(productData.Price),
        jumlah: parseInt(quantity),
        status: 'PENDING',
      },
    };
    axios(config)
      .then((response) => {
        console.log(response.data);
        router.push(`/product/list-user`);
      })
      .catch((error) => {
        alert('error when creating transaction');
        alert(error);
      });
  };

  const onCancel = () => {
    router.push('/product/list-user');
  };

  useEffect(() => {
    if (session && username == '') {
      if (session?.data?.username != '') {
        setUsername(session?.data?.username);
        console.log(session?.data);
        console.log('username ku adalah');
        console.log(session?.data?.username);
      }
    }
    // get product
    const getProduct = () => {
      const config = {
        method: 'get',
        url: dbStokPath + '/product/' + parseInt(ID),
      };
      axios(config)
        .then((response) => {
          setProductData(response.data.data);
          setTotalPrice(response.data.data.Price);
        })
        .catch((error) => {
          alert(error);
          alert('error when fetching product data');
        });
    };

    if (!productData.length) getProduct();
  }, [productData]);

  const NumberInput = ({ value }) => {
    return (
      <div className="flex flex-row items-center">
        <button
          className={`${styles.primaryButton}`}
          style={style.iconButton}
          onClick={decrement}
        >
          -
        </button>
        <input
          style={style.numInput}
          type="number"
          value={value}
          defaultValue={1}
          readOnly
        />
        <button
          className={`${styles.primaryButton}`}
          style={style.iconButton}
          onClick={increment}
        >
          +
        </button>
      </div>
    );
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>{title} | Toko LAWas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <div className="text-center">
          <h1 className={styles.heading}>{title}</h1>
        </div>
        <div className="flex flex-row mx-4">
          <section className="flex flex-col flex-grow">
            <div className="font-bold">{productData.Name}</div>
            <hr style={style.divider} />
            <div>Rp {productData.Price}</div>
            <div>Quantity in Stock: {productData.Quantity}</div>
            <hr style={style.divider} />
            <div className="font-bold">Buy Item</div>
            <div>
              <NumberInput style={style.numInput} value={quantity} />
              <span>
                <b>Total:</b> Rp {totalPrice}
              </span>
              <hr style={style.divider} />
              <button className={`${styles.primaryButton}`} onClick={onSubmit}>
                Submit
              </button>
              <button className={`${styles.primaryButton}`} onClick={onCancel}>
                Cancel
              </button>
            </div>
          </section>
        </div>
      </main>

      <footer className={styles.footer}>
        <a target="_blank" rel="noopener noreferrer">
          Powered by Seblak
        </a>
      </footer>
    </div>
  );
}

const style = {
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'start',
    grow: 1,
    justifyContent: 'center',
    borderRadius: '5px',
    padding: '1rem',
  },
  divider: {
    borderBottom: '1px solid #bbb',
  },
  numInput: {
    border: 'none',
    height: '2rem',
    textAlign: 'center',
  },
};
