import axios from "axios"
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react"
import styles from "../styles/Home.module.css"

export default function Notification() {
    const { data: session } = useSession();
    const [notifications, setNotifications] = useState([])

    useEffect(() => {
        axios.post('http://35.223.24.101:4001/get', {
            "username": session?.user.username,
            "secret": "pakabarGeng?moga2AmanLAWya",
        })
            .then((response) => setNotifications(response.data.notifications?.reverse()))
    },[])

    return <main>
        <div className="container pb-8">
            <div className="text-center">
                <h1 className={styles.heading}>Notifications</h1>
            </div>
            {notifications.map((notification, index) =>
                <div className={(notification.is_new ? "bg-sky-200" : "bg-white") + " w-100 rounded p-3 mb-3"} key={index}>
                    <div className="font-bold">{notification.title}</div>
                    <div>{notification.content}</div>
                </div>
            )}
        </div>
    </main>
}